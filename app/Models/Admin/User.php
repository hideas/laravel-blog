<?php

namespace App\Models\Admin;

use Illuminate\Contracts\Eloquent\Model;

class User extends Model
{
    protected $fillable = [
        'name', 'email', 'password', 'login',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datatime',
    ];

    public function roles() {
        return $this->belongsToMany('App\Models\Role', 'user_roles');
    }
}